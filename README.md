# PureCloud Activation Tool

This application allows a PureCloud user to deactivate and re-activate queue members from PureCloud.
This was created to workaround an existing issue (SERVOPS-4517) before a fix is delivered

![picture](resources/Screenshot.jpg)

## Requirements
*  .Net 4.5.2
*  Access to https://api.mypurecloud.ie (or .com, .de, .jp, .com.au depending on your environment) without requiring a proxy
*  Administrative access to the machine you are going to install this application on
*  A `client id` and `client secret` using `Client Credentials`

## Installation
*  Download the latest setup program [here](https://bitbucket.org/eccemea/pc-activation-tool/downloads/PC%20Activation%20Tool%20Setup.msi) & run it
*  Follow the wizard
*  A desktop shortcut is also installed

## Usage
*  Open the application (e.g. C:\Program Files (x86)\PCActivationTool\PCActivationTool.exe) or use the Desktop shortcut
*  Select your PureCloud environment
*  Paste your `client id` and `client secret` and the corresponding text fields
*  Click on `Connect`
*  Select a queue
*  Click on `Deactivate All Members` to deactivate the currently activated queue members
*  Click on `Activate All Members` to reactivate the queue members

# *IMPORTANT*

*DO NOT CHANGE* the selected queue or exit the application after deactivating the queue members and before reactivating them otherwise the list of previously de-activated members will be lost
