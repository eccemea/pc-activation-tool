﻿namespace PCActivationTool
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.gbConnection = new System.Windows.Forms.GroupBox();
			this.btnDisconnect = new System.Windows.Forms.Button();
			this.btnConnect = new System.Windows.Forms.Button();
			this.cmbEnvironment = new System.Windows.Forms.ComboBox();
			this.lblEnvironment = new System.Windows.Forms.Label();
			this.txtClientSecret = new System.Windows.Forms.TextBox();
			this.lblClientSecret = new System.Windows.Forms.Label();
			this.txtClientId = new System.Windows.Forms.TextBox();
			this.lblClientId = new System.Windows.Forms.Label();
			this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
			this.lstLog = new System.Windows.Forms.ListBox();
			this.gbQueueActivation = new System.Windows.Forms.GroupBox();
			this.btnActivate = new System.Windows.Forms.Button();
			this.btnDeactivate = new System.Windows.Forms.Button();
			this.cmbQueues = new System.Windows.Forms.ComboBox();
			this.lblQueues = new System.Windows.Forms.Label();
			this.btnClearLog = new System.Windows.Forms.Button();
			this.gbConnection.SuspendLayout();
			this.gbQueueActivation.SuspendLayout();
			this.SuspendLayout();
			// 
			// gbConnection
			// 
			this.gbConnection.Controls.Add(this.btnDisconnect);
			this.gbConnection.Controls.Add(this.btnConnect);
			this.gbConnection.Controls.Add(this.cmbEnvironment);
			this.gbConnection.Controls.Add(this.lblEnvironment);
			this.gbConnection.Controls.Add(this.txtClientSecret);
			this.gbConnection.Controls.Add(this.lblClientSecret);
			this.gbConnection.Controls.Add(this.txtClientId);
			this.gbConnection.Controls.Add(this.lblClientId);
			this.gbConnection.Location = new System.Drawing.Point(0, 0);
			this.gbConnection.Name = "gbConnection";
			this.gbConnection.Size = new System.Drawing.Size(316, 132);
			this.gbConnection.TabIndex = 0;
			this.gbConnection.TabStop = false;
			this.gbConnection.Text = "PureCloud Settings";
			// 
			// btnDisconnect
			// 
			this.btnDisconnect.Enabled = false;
			this.btnDisconnect.Location = new System.Drawing.Point(164, 98);
			this.btnDisconnect.Name = "btnDisconnect";
			this.btnDisconnect.Size = new System.Drawing.Size(75, 23);
			this.btnDisconnect.TabIndex = 9;
			this.btnDisconnect.Text = "Disconnect";
			this.btnDisconnect.UseVisualStyleBackColor = true;
			this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
			// 
			// btnConnect
			// 
			this.btnConnect.Location = new System.Drawing.Point(83, 98);
			this.btnConnect.Name = "btnConnect";
			this.btnConnect.Size = new System.Drawing.Size(75, 23);
			this.btnConnect.TabIndex = 8;
			this.btnConnect.Text = "Connect";
			this.btnConnect.UseVisualStyleBackColor = true;
			this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
			// 
			// cmbEnvironment
			// 
			this.cmbEnvironment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbEnvironment.FormattingEnabled = true;
			this.cmbEnvironment.Items.AddRange(new object[] {
            "mypurecloud.com",
            "mypurecloud.com.au",
            "mypurecloud.de",
            "mypurecloud.ie",
            "mypurecloud.jp"});
			this.cmbEnvironment.Location = new System.Drawing.Point(83, 19);
			this.cmbEnvironment.Name = "cmbEnvironment";
			this.cmbEnvironment.Size = new System.Drawing.Size(136, 21);
			this.cmbEnvironment.TabIndex = 7;
			// 
			// lblEnvironment
			// 
			this.lblEnvironment.AutoSize = true;
			this.lblEnvironment.Location = new System.Drawing.Point(8, 22);
			this.lblEnvironment.Name = "lblEnvironment";
			this.lblEnvironment.Size = new System.Drawing.Size(69, 13);
			this.lblEnvironment.TabIndex = 6;
			this.lblEnvironment.Text = "Environment:";
			// 
			// txtClientSecret
			// 
			this.txtClientSecret.Location = new System.Drawing.Point(83, 72);
			this.txtClientSecret.Name = "txtClientSecret";
			this.txtClientSecret.PasswordChar = '*';
			this.txtClientSecret.Size = new System.Drawing.Size(224, 20);
			this.txtClientSecret.TabIndex = 5;
			// 
			// lblClientSecret
			// 
			this.lblClientSecret.AutoSize = true;
			this.lblClientSecret.Location = new System.Drawing.Point(7, 75);
			this.lblClientSecret.Name = "lblClientSecret";
			this.lblClientSecret.Size = new System.Drawing.Size(70, 13);
			this.lblClientSecret.TabIndex = 4;
			this.lblClientSecret.Text = "Client Secret:";
			// 
			// txtClientId
			// 
			this.txtClientId.Location = new System.Drawing.Point(83, 46);
			this.txtClientId.Name = "txtClientId";
			this.txtClientId.Size = new System.Drawing.Size(224, 20);
			this.txtClientId.TabIndex = 3;
			// 
			// lblClientId
			// 
			this.lblClientId.AutoSize = true;
			this.lblClientId.Location = new System.Drawing.Point(29, 49);
			this.lblClientId.Name = "lblClientId";
			this.lblClientId.Size = new System.Drawing.Size(48, 13);
			this.lblClientId.TabIndex = 2;
			this.lblClientId.Text = "Client Id:";
			// 
			// lstLog
			// 
			this.lstLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lstLog.FormattingEnabled = true;
			this.lstLog.Location = new System.Drawing.Point(0, 144);
			this.lstLog.Name = "lstLog";
			this.lstLog.Size = new System.Drawing.Size(694, 290);
			this.lstLog.TabIndex = 1;
			// 
			// gbQueueActivation
			// 
			this.gbQueueActivation.Controls.Add(this.btnActivate);
			this.gbQueueActivation.Controls.Add(this.btnDeactivate);
			this.gbQueueActivation.Controls.Add(this.cmbQueues);
			this.gbQueueActivation.Controls.Add(this.lblQueues);
			this.gbQueueActivation.Enabled = false;
			this.gbQueueActivation.Location = new System.Drawing.Point(322, 6);
			this.gbQueueActivation.Name = "gbQueueActivation";
			this.gbQueueActivation.Size = new System.Drawing.Size(371, 82);
			this.gbQueueActivation.TabIndex = 2;
			this.gbQueueActivation.TabStop = false;
			this.gbQueueActivation.Text = "Queue Activation";
			// 
			// btnActivate
			// 
			this.btnActivate.Location = new System.Drawing.Point(222, 46);
			this.btnActivate.Name = "btnActivate";
			this.btnActivate.Size = new System.Drawing.Size(140, 23);
			this.btnActivate.TabIndex = 12;
			this.btnActivate.Text = "Activate All Members";
			this.btnActivate.UseVisualStyleBackColor = true;
			this.btnActivate.Click += new System.EventHandler(this.btnActivate_Click);
			// 
			// btnDeactivate
			// 
			this.btnDeactivate.Location = new System.Drawing.Point(61, 46);
			this.btnDeactivate.Name = "btnDeactivate";
			this.btnDeactivate.Size = new System.Drawing.Size(140, 23);
			this.btnDeactivate.TabIndex = 11;
			this.btnDeactivate.Text = "Deactivate All Members";
			this.btnDeactivate.UseVisualStyleBackColor = true;
			this.btnDeactivate.Click += new System.EventHandler(this.btnDeactivate_Click);
			// 
			// cmbQueues
			// 
			this.cmbQueues.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbQueues.FormattingEnabled = true;
			this.cmbQueues.Location = new System.Drawing.Point(61, 19);
			this.cmbQueues.Name = "cmbQueues";
			this.cmbQueues.Size = new System.Drawing.Size(301, 21);
			this.cmbQueues.TabIndex = 10;
			this.cmbQueues.SelectedIndexChanged += new System.EventHandler(this.cmbQueues_SelectedIndexChanged);
			// 
			// lblQueues
			// 
			this.lblQueues.AutoSize = true;
			this.lblQueues.Location = new System.Drawing.Point(8, 22);
			this.lblQueues.Name = "lblQueues";
			this.lblQueues.Size = new System.Drawing.Size(47, 13);
			this.lblQueues.TabIndex = 0;
			this.lblQueues.Text = "Queues:";
			// 
			// btnClearLog
			// 
			this.btnClearLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnClearLog.Location = new System.Drawing.Point(618, 109);
			this.btnClearLog.Name = "btnClearLog";
			this.btnClearLog.Size = new System.Drawing.Size(75, 23);
			this.btnClearLog.TabIndex = 3;
			this.btnClearLog.Text = "Clear Log";
			this.btnClearLog.UseVisualStyleBackColor = true;
			this.btnClearLog.Click += new System.EventHandler(this.btnClearLog_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(697, 437);
			this.Controls.Add(this.btnClearLog);
			this.Controls.Add(this.gbQueueActivation);
			this.Controls.Add(this.lstLog);
			this.Controls.Add(this.gbConnection);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "Form1";
			this.Text = "PureCloud Activation Tool";
			this.gbConnection.ResumeLayout(false);
			this.gbConnection.PerformLayout();
			this.gbQueueActivation.ResumeLayout(false);
			this.gbQueueActivation.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox gbConnection;
		private System.Windows.Forms.ComboBox cmbEnvironment;
		private System.Windows.Forms.Label lblEnvironment;
		private System.Windows.Forms.TextBox txtClientSecret;
		private System.Windows.Forms.Label lblClientSecret;
		private System.Windows.Forms.TextBox txtClientId;
		private System.Windows.Forms.Label lblClientId;
		private System.ComponentModel.BackgroundWorker backgroundWorker1;
		private System.Windows.Forms.Button btnConnect;
		private System.Windows.Forms.ListBox lstLog;
		private System.Windows.Forms.Button btnDisconnect;
		private System.Windows.Forms.GroupBox gbQueueActivation;
		private System.Windows.Forms.ComboBox cmbQueues;
		private System.Windows.Forms.Label lblQueues;
		private System.Windows.Forms.Button btnActivate;
		private System.Windows.Forms.Button btnDeactivate;
		private System.Windows.Forms.Button btnClearLog;
	}
}

