﻿using PureCloudPlatform.Client.V2.Api;
using PureCloudPlatform.Client.V2.Client;
using PureCloudPlatform.Client.V2.Extensions;
using PureCloudPlatform.Client.V2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace PCActivationTool
{
	public partial class Form1 : Form
	{
		QueueInfo currentQueue = null;
		List<QueueMember> currentQueueMembers = new List<QueueMember>();
		RoutingApi routingApi = null;
		bool loggedIn = false;

		#region Initialization

		public Form1()
		{
			InitializeComponent();
			cmbEnvironment.SelectedIndex = 0;
			AddLog("Initializing");

			Application.ApplicationExit += (sender, e) =>
			{
				Disconnect();
			};
		}

		#endregion

		#region Connection

		private void btnConnect_Click(object sender, EventArgs e)
		{
			try
			{
				// Connect to PureCloud
				Configuration.Default.ApiClient.RestClient.BaseUrl = new Uri($"https://api.{cmbEnvironment.SelectedItem.ToString()}");
				var accessTokenInfo = Configuration.Default.ApiClient.PostToken(txtClientId.Text, txtClientSecret.Text);
				Configuration.Default.AccessToken = accessTokenInfo.AccessToken;
				loggedIn = true;
				AddLog($"Access Token: {accessTokenInfo.AccessToken}");

				// Update Controls
				btnConnect.Enabled = false;
				btnDisconnect.Enabled = true;
				gbQueueActivation.Enabled = btnDisconnect.Enabled;

				// Populate Queues
				GetQueues();
			}
			catch (Exception ex)
			{
				AddLog($"Error in btnConnect_Click: {ex.Message}");
				MessageBox.Show("Have you selected the correct environment?", "Could not connect", MessageBoxButtons.OK, MessageBoxIcon.Error);
				btnConnect.Enabled = true;
				btnDisconnect.Enabled = false;
				gbQueueActivation.Enabled = btnDisconnect.Enabled;
				loggedIn = false;
			}
		}

		private void btnDisconnect_Click(object sender, EventArgs e)
		{
			Disconnect();
		}

		private void Disconnect()
		{
			if (loggedIn)
			{
				var tokensApi = new TokensApi();
				AddLog("Disconnecting...");
				tokensApi.DeleteTokensMe();
				loggedIn = false;
			}
			btnConnect.Enabled = true;
			btnDisconnect.Enabled = false;
			gbQueueActivation.Enabled = btnDisconnect.Enabled;
			cmbQueues.Items.Clear();
		}

		#endregion

		#region Queues

		private void GetQueues()
		{
			try
			{
				routingApi = new RoutingApi();
				var pageNumber = 1;
				var pageCount = 1;

				AddLog($"Getting Queues");
				do
				{
					var queueEntityListing = routingApi.GetRoutingQueues(100, pageNumber++, null, null, true);
					pageCount = queueEntityListing.PageCount.Value;
					foreach (var queue in queueEntityListing.Entities)
					{
						cmbQueues.Items.Add(new QueueInfo(queue.Id, queue.Name, queue.MemberCount));
					}
				} while (pageNumber <= pageCount && loggedIn);
				cmbQueues.SelectedIndex = 0;
			}
			catch (Exception ex)
			{
				AddLog($"Error in GetQueues: {ex.Message}");
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void cmbQueues_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				var queue = (QueueInfo)((ComboBox)sender).SelectedItem;
				currentQueue = queue;

				// Reset values
				if (currentQueue.AllQueueMembers == null)
				{
					currentQueue.AllQueueMembers = new List<QueueMember>();
				}
				else
				{
					currentQueue.AllQueueMembers.Clear();
				}

				currentQueueMembers.Clear();

				// Get List of All Members
				AddLog($"Getting all {queue.Name} members");

				var pageNumber = 1;
				var pageCount = 1;

				do
				{
					var allQueueMemberEntityListing = routingApi.GetRoutingQueueUsers(currentQueue.Id, 100, pageNumber++, "name");
					pageCount = allQueueMemberEntityListing.PageCount.Value;
					currentQueue.AllQueueMembers.AddRange(allQueueMemberEntityListing.Entities);
				} while (pageNumber <= pageCount && loggedIn);

				// Get Active Members
				AddLog($"Getting all {currentQueue.Name} active members");

				var queueMemberEntityListing = new QueueMemberEntityListing();
				pageNumber = 1;
				pageCount = 1;
				
				do
				{
					queueMemberEntityListing = routingApi.GetRoutingQueueUsers(currentQueue.Id, 100, pageNumber++, "name", null, true);
					pageCount = queueMemberEntityListing.PageCount.Value;
					currentQueueMembers.AddRange(queueMemberEntityListing.Entities);
				} while (pageNumber <= pageCount && loggedIn);

				AddLog($"{currentQueue} has {currentQueueMembers.Count} activated member(s):");
				if (currentQueueMembers.Count == 0)
				{
					AddLog($"If you click on Activate now, all {currentQueue} members will be activated");
				}

				foreach (var queueMember in currentQueueMembers)
				{
					AddLog($"Queue Member: {queueMember.Name}");
				}
			}
			catch (Exception ex)
			{
				AddLog($"Error in cmbQueues_SelectedIndexChanged: {ex}");
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		#endregion

		#region Activation/Deactivation

		private void btnDeactivate_Click(object sender, EventArgs e)
		{
			try
			{
				int deactivatedCount = 0;

				// Send Patch request to apply the changes. No more than 100 agents can be deactivated in a single request
				for(int i = 0; i < currentQueueMembers.Count; i=i+100)
				{
					var queueMembersToDeactivate = currentQueueMembers.Skip(i).Take(100).ToList();
					var queueMemberEntityListing = routingApi.PatchRoutingQueueUsers(currentQueue.Id, queueMembersToDeactivate);
					deactivatedCount += queueMembersToDeactivate.Count;
					foreach (var queueMember in queueMembersToDeactivate)
					{
						AddLog($"{queueMember.Name} has been deactivated from {currentQueue.Name}");
					}
				}

				// Mark all current queue members as unjoined
				foreach (var queueMember in currentQueueMembers)
				{
					queueMember.Joined = false;
				}

				AddLog($"{deactivatedCount} members have been deactivated from {currentQueue.Name}");
			}
			catch (Exception ex)
			{
				AddLog($"Error in btnDeactivate_Click: {ex.Message}");
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void btnActivate_Click(object sender, EventArgs e)
		{
			try
			{
				int activatedCount = 0;

				// Activate all members if we do not have a current count
				if (currentQueueMembers.Count == 0)
				{
					currentQueueMembers = ((QueueInfo)cmbQueues.SelectedItem).AllQueueMembers;
				}

				// Send Patch request to apply the changes. No more than 100 agents can be activated in a single request
				for (int i = 0; i < currentQueueMembers.Count; i = i + 100)
				{
					var queueMembersToActivate = currentQueueMembers.Skip(i).Take(100).ToList();
					var queueMemberEntityListing = routingApi.PatchRoutingQueueUsers(currentQueue.Id, queueMembersToActivate);
					activatedCount += queueMembersToActivate.Count;
					foreach (var queueMember in queueMembersToActivate)
					{
						AddLog($"{queueMember.Name} is now activated in {currentQueue.Name}");
					}
				}

				// Mark all current queue members as joined
				foreach (var queueMember in currentQueueMembers)
				{
					queueMember.Joined = true;
				}

				AddLog($"{activatedCount} members have been activated in {currentQueue.Name}");
			}
			catch (Exception ex)
			{
				AddLog($"Error in btnDeactivate_Click: {ex.Message}");
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		#endregion

		#region Log

		private void AddLog(string message)
		{
			lstLog.Items.Add($"{DateTime.Now} {message}");
		}

		private void btnClearLog_Click(object sender, EventArgs e)
		{
			lstLog.Items.Clear();
		}

		#endregion
	}
}
