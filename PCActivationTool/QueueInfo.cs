﻿using PureCloudPlatform.Client.V2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCActivationTool
{
	public class QueueInfo
	{
		public String Id { get; set; }
		public String Name { get; set; }
		public int? MemberCount { get; set; }
		public List<QueueMember> AllQueueMembers { get; set; }

		public override string ToString()
		{
			return Name;
		}

		public QueueInfo(string id, string name, int? memberCount)
		{
			Id = id;
			Name = name;
			MemberCount = memberCount;
		}
	}
}
